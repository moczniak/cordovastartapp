var exec = require('cordova/exec');

exports.coolMethod = function(arg0, success, error) {
    exec(success, error, "StartAppAds", "coolMethod", [arg0]);
};


exports.init = function(arg0) {
    exec(null, null, "StartAppAds", "init", [arg0]);
};


exports.showBanner = function() {
    exec(null, null, "StartAppAds", "showBanner", null);
};

exports.showIntersitial = function() {
    exec(null, null, "StartAppAds", "showIntersitial", null);
};