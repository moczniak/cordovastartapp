package com.moczniak.startappads;
/*
import org.apache.cordova.*;
import org.apache.cordova.PluginResult.Status;
*/
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaWebView;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.startapp.android.publish.adsCommon.StartAppSDK;

import com.startapp.android.publish.ads.banner.Banner;
import com.startapp.android.publish.adsCommon.StartAppAd;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.RelativeLayout;

/**
 * This class echoes a string called from JavaScript.
 */
public class StartAppAds extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("coolMethod")) {
            String message = args.getString(0);
            this.coolMethod(message, callbackContext);
            return true;
        }
        if (action.equals("init")) {
            String appID = args.getString(0);
            this.init(appID);
            return true;
        }
        if (action.equals("showBanner")) {
            this.showBanner();
			//this.cordova.getActivity().getApplicationContext(), this.cordova.getActivity()
            return true;
        }
        if (action.equals("showIntersitial")) {
            this.showIntersitial();
			//this.cordova.getActivity().getApplicationContext(), this.cordova.getActivity()
            return true;
        }
        return false;
    }

    private void coolMethod(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
	
    private void init(final String appID) {
		final Context cordovaContext = this.cordova.getActivity().getApplicationContext();
        this.cordova.getThreadPool().execute(new Runnable() {
            public void run() {
				StartAppSDK.init(cordovaContext, appID, true);
            }
        });
    }
	
    private void showBanner() {
		/*
		final Context cordovaContext = this.cordova.getActivity().getApplicationContext(); 
		final Activity cordovaActivity = this.cordova.getActivity();
		
        this.cordova.getActivity().runOnUiThread(new Runnable(){
            @Override
            public void run() {
				RelativeLayout mainLayout = new RelativeLayout(cordovaActivity);
				Banner startAppBanner = new Banner(cordovaContext);
	
				RelativeLayout.LayoutParams bannerParameters = 
						new RelativeLayout.LayoutParams(
								RelativeLayout.LayoutParams.MATCH_PARENT,
								RelativeLayout.LayoutParams.WRAP_CONTENT);
								
								
								
				bannerParameters.addRule(RelativeLayout.CENTER_HORIZONTAL);
				bannerParameters.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
				
				// Add the banner to the main layout
				mainLayout.addView(startAppBanner, bannerParameters);
				
				RelativeLayout.LayoutParams bannerParametersOuter = 
						new RelativeLayout.LayoutParams(
								RelativeLayout.LayoutParams.MATCH_PARENT,
								RelativeLayout.LayoutParams.MATCH_PARENT);
								
				RelativeLayout adViewLayout = new RelativeLayout(cordovaActivity);
				((ViewGroup) webView).addView(adViewLayout, bannerParametersOuter);
				adViewLayout.addView(adView, bannerParameters);
				adViewLayout.bringToFront();
            }
        });
		*/
    }
	
	private void showIntersitial() {
		final Context cordovaContext = this.cordova.getActivity().getApplicationContext(); 

        this.cordova.getActivity().runOnUiThread(new Runnable(){
            @Override
            public void run() {
				StartAppAd.showAd(cordovaContext);
            }	
        });
		
	}
	
	
	
}
